-- This is the primary junglebash junglebash script and should be used to assist in initializing your game mode
JUNGLEBASH_VERSION = "1.00"

-- Set this to true if you want to see a complete debug output of all events/processes done by junglebash
-- You can also change the cvar 'junglebash_spew' at any time to 1 or 0 for output/no output
JUNGLEBASH_DEBUG_SPEW = false 

if junglebash == nil then
    DebugPrint( '[JUNGLEBASH] creating junglebash game mode' )
    _G.junglebash = class({})
end

-- This library allow for easily delayed/timed actions
require('libraries/timers')
-- This library can be used for advancted physics/motion/collision of units.  See PhysicsReadme.txt for more information.
require('libraries/physics')
-- This library can be used for advanced 3D projectile systems.
require('libraries/projectiles')
-- This library can be used for sending panorama notifications to the UIs of players/teams/everyone
require('libraries/notifications')
-- This library can be used for starting customized animations on units from lua
require('libraries/animations')
-- This library can be used for performing "Frankenstein" attachments on units
require('libraries/attachments')
-- This library can be used to synchronize client-server data via player/client-specific nettables
require('libraries/playertables')
-- This library can be used to create container inventories or container shops
require('libraries/containers')
-- This library provides a searchable, automatically updating lua API in the tools-mode via "modmaker_api" console command
require('libraries/modmaker')
-- This library provides an automatic graph construction of path_corner entities within the map
require('libraries/pathgraph')
-- This library (by Noya) provides player selection inspection and management from server lua
require('libraries/selection')

-- These internal libraries set up junglebash's events and processes.  Feel free to inspect them/change them if you need to.
require('internal/junglebash')
require('internal/events')

-- settings.lua is where you can specify many different properties for your game mode and is one of the core junglebash files.
require('settings')
-- events.lua is where you can specify the actions to be taken when any event occurs and is one of the core junglebash files.
require('events')


-- This is a detailed example of many of the containers.lua possibilities, but only activates if you use the provided "playground" map
if GetMapName() == "playground" then
  require("examples/playground")
end

--require("examples/worldpanelsExample")

--[[
  This function should be used to set up Async precache calls at the beginning of the gameplay.

  In this function, place all of your PrecacheItemByNameAsync and PrecacheUnitByNameAsync.  These calls will be made
  after all players have loaded in, but before they have selected their heroes. PrecacheItemByNameAsync can also
  be used to precache dynamically-added datadriven abilities instead of items.  PrecacheUnitByNameAsync will 
  precache the precache{} block statement of the unit and all precache{} block statements for every Ability# 
  defined on the unit.

  This function should only be called once.  If you want to/need to precache more items/abilities/units at a later
  time, you can call the functions individually (for example if you want to precache units in a new wave of
  holdout).

  This function should generally only be used if the Precache() function in addon_game_mode.lua is not working.
]]
function junglebash:PostLoadPrecache()
  DebugPrint("[JUNGLEBASH] Performing Post-Load precache")    
  --PrecacheItemByNameAsync("item_example_item", function(...) end)
  --PrecacheItemByNameAsync("example_ability", function(...) end)

  --PrecacheUnitByNameAsync("npc_dota_hero_viper", function(...) end)
  --PrecacheUnitByNameAsync("npc_dota_hero_enigma", function(...) end)
end

--[[
  This function is called once and only once as soon as the first player (almost certain to be the server in local lobbies) loads in.
  It can be used to initialize state that isn't initializeable in Initjunglebash() but needs to be done before everyone loads in.
]]
function junglebash:OnFirstPlayerLoaded()
  DebugPrint("[JUNGLEBASH] First Player has loaded")
end

--[[
  This function is called once and only once after all players have loaded into the game, right as the hero selection time begins.
  It can be used to initialize non-hero player state or adjust the hero selection (i.e. force random etc)
]]
function junglebash:OnAllPlayersLoaded()
  DebugPrint("[JUNGLEBASH] All Players have loaded into the game")
end

--[[
  This function is called once and only once for every player when they spawn into the game for the first time.  It is also called
  if the player's hero is replaced with a new hero for any reason.  This function is useful for initializing heroes, such as adding
  levels, changing the starting gold, removing/adding abilities, adding physics, etc.

  The hero parameter is the hero entity that just spawned in
]]
function junglebash:OnHeroInGame(hero)
  DebugPrint("[JUNGLEBASH] Hero spawned in game for first time -- " .. hero:GetUnitName())

  -- This line for example will set the starting gold of every hero to 500 unreliable gold
  --hero:SetGold(500, false)

  -- These lines will create an item and add it to the player, effectively ensuring they start with the item
  local item = CreateItem("item_example_item", hero, hero)
  hero:AddItem(item)

  --[[ --These lines if uncommented will replace the W ability of any hero that loads into the game
    --with the "example_ability" ability

  local abil = hero:GetAbilityByIndex(1)
  hero:RemoveAbility(abil:GetAbilityName())
  hero:AddAbility("example_ability")]]
end

--[[
  This function is called once and only once when the game completely begins (about 0:00 on the clock).  At this point,
  gold will begin to go up in ticks if configured, creeps will spawn, towers will become damageable etc.  This function
  is useful for starting any game logic timers/thinkers, beginning the first round, etc.
]]
function junglebash:OnGameInProgress()
  DebugPrint("[JUNGLEBASH] The game has officially begun")
    local repeat_interval = 240 -- Rerun this timer every *repeat_interval* game-time seconds
    local start_after = 0 -- Start this timer *start_after* game-time seconds later
    print ("bounty rune apawned!")
	EmitAnnouncerSound("announcer_announcer_welcome_07")
    Timers:CreateTimer(start_after, function()
        spawnrunes()
        return repeat_interval
    end)
	function spawnrunes()
	print ("testing")
    local point = Entities:FindByName( nil, "rapierspawner1"):GetAbsOrigin()
    local unit = CreateUnitByName("Bounty_Creep", point, true, nil, nil, DOTA_TEAM_CUSTOM_3)
	    local point = Entities:FindByName( nil, "rapierspawner2"):GetAbsOrigin()
		EmitGlobalSound("General.CoinsBig")
    local unit = CreateUnitByName("Bounty_Creep", point, true, nil, nil, DOTA_TEAM_CUSTOM_3)
	    local point = Entities:FindByName( nil, "rapierspawner3"):GetAbsOrigin()
    local unit = CreateUnitByName("Bounty_Creep", point, true, nil, nil, DOTA_TEAM_CUSTOM_3)
	    local point = Entities:FindByName( nil, "rapierspawner4"):GetAbsOrigin()
    local unit = CreateUnitByName("Bounty_Creep", point, true, nil, nil, DOTA_TEAM_CUSTOM_3)
	    local point = Entities:FindByName( nil, "rapierspawner5"):GetAbsOrigin()
    local unit = CreateUnitByName("Bounty_Creep", point, true, nil, nil, DOTA_TEAM_CUSTOM_3)
	    local point = Entities:FindByName( nil, "rapierspawner6"):GetAbsOrigin()
    local unit = CreateUnitByName("Bounty_Creep", point, true, nil, nil, DOTA_TEAM_CUSTOM_3)
	Notifications:TopToAll({text="Midas' Sheep Have Spawned", duration=9, style={color="gold", ["font-size"]="40px", border="0px  megenta"}})
	end

	
	
	local repeat_interval = 240 -- Rerun this timer every *repeat_interval* game-time seconds
	local start_after = 120 -- Start this timer *start_after* game-time seconds later
    print ("Timer 2 is running")
    Timers:CreateTimer(start_after, function()
        Spawndamagesheep()
		return repeat_interval
        
    end)

-- testing
function Spawndamagesheep()
print ("sheepies spawned in the game now!")
	local point = Entities:FindByName( nil, "damage1"):GetAbsOrigin()
	EmitGlobalSound("Tower.Water.Attack")
    local unit = CreateUnitByName("damage_Creep", point, true, nil, nil, DOTA_TEAM_NEUTRALS)
	unit:AddItem(CreateItem("item_flask_datadriven", unit, unit))
	Notifications:TopToAll({text="Underworld Fish Have Arrived", duration=9, style={color="green", ["font-size"]="40px", border="0px  megenta"}})
	--EmitGlobalSound("CNY_Beast.GlobalSilence.Effect")
	local point = Entities:FindByName( nil, "damage2"):GetAbsOrigin()
    local unit = CreateUnitByName("damage_Creep", point, true, nil, nil, DOTA_TEAM_NEUTRALS)
	unit:AddItem(CreateItem("item_flask_datadriven", unit, unit))
end


	
	local start_after = 660 -- Start this timer *start_after* game-time seconds later
    print ("Timer 2 is running")
    Timers:CreateTimer(start_after, function()
        fishofdeath()

        
    end)
	function fishofdeath()
	print ("fish has spawned into the game")
	local point = Entities:FindByName( nil, "fish_of_death"):GetAbsOrigin()
    local waypoint = Entities:FindByName(nil, "fish1")
    local unit = CreateUnitByName("fish_of_death", point, true, nil, nil, DOTA_TEAM_NEUTRALS)
    unit:SetInitialGoalEntity( waypoint )
	unit:AddItem(CreateItem("item_vitality_booster_datadriven", unit, unit))
	Notifications:TopToAll({text="Black Fin has Arrived! (check the river)", duration=9, style={color="magenta", ["font-size"]="45px", border="0px  megenta"}})
	Notifications:TopToAll({text="Wolves are on the prowl! Kill them before they take your camps!", duration=9, style={color="red", ["font-size"]="45px", border="0px  megenta"}})
	EmitGlobalSound("arc_warden_debut_takeover_stinger")
	unit:SetUnitName("fish_of_death")
	end
	
		local repeat_interval = 300 -- Rerun this timer every *repeat_interval* game-time seconds
	local start_after = 300  -- Start this timer *start_after* game-time seconds later
    print ("Timer 2 is running")
    Timers:CreateTimer(start_after, function()
        Spawngreevil()
		return repeat_interval   
    end)
	function Spawngreevil()
		Notifications:TopToAll({text="Get The Pesky Greevils!", duration=9, style={color="blue", ["font-size"]="45px", border="0px  megenta"}})
		local point = Entities:FindByName( nil, "invis1"):GetAbsOrigin()
		local unit = CreateUnitByName("invis_greevil", point, true, nil, nil, DOTA_TEAM_NEUTRALS)
		unit:AddNewModifier(unit, nil, "modifier_invisible", {fadetime = 0})
		unit:AddItem(CreateItem("item_invis_datadriven", unit, unit))
		local point = Entities:FindByName( nil, "invis2"):GetAbsOrigin()
		local unit = CreateUnitByName("invis_greevil", point, true, nil, nil, DOTA_TEAM_NEUTRALS)
		unit:AddNewModifier(unit, nil, "modifier_invisible", {fadetime = 0})
		unit:AddItem(CreateItem("item_invis_datadriven", unit, unit))
		end


    local start_after = 1260 -- Start this timer *start_after* game-time seconds later
    print ("Timer 2 is running")
    Timers:CreateTimer(start_after, function()
        iceking()
        end)


function iceking()
  print ("Ice King has spawned into the game")
  local point = Entities:FindByName( nil, "ice_king"):GetAbsOrigin()
    local waypoint = Entities:FindByName(nil, "ice1")
    local unit = CreateUnitByName("frost_king", point, true, nil, nil, DOTA_TEAM_NEUTRALS)
    unit:SetInitialGoalEntity( waypoint )
    unit:AddItem(CreateItem("item_vitality_booster_datadriven", unit, unit))
    Notifications:TopToAll({text="The Crystal Sage has Arrived! (check near Roshan!)", duration=9, style={color="magenta", ["font-size"]="45px", border="0px  megenta"}})
    EmitGlobalSound("Greevil.IceWall.Slow")
    unit:SetUnitName("ice_king")
  end

  		

		  		local repeat_interval = 120 -- Rerun this timer every *repeat_interval* game-time seconds
	local start_after = 0  -- Start this timer *start_after* game-time seconds later
    print ("Timer 2 is running")
    Timers:CreateTimer(start_after, function()
        roaming2()
		return repeat_interval   
    end)
	function roaming2()
		
		local point = GetGroundPosition(RandomVector(8000), nil)
		local unit = CreateUnitByName("alpha_roaming_wolf", point, true, nil, nil, DOTA_TEAM_BADGUYS)
		local waypoint = Entities:FindByName(nil, "wolf7")
		unit:SetInitialGoalEntity( waypoint )
				local unit = CreateUnitByName("roaming_wolf", point, true, nil, nil, DOTA_TEAM_BADGUYS)
		local waypoint = Entities:FindByName(nil, "wolf7")
		unit:SetInitialGoalEntity( waypoint )
				local unit = CreateUnitByName("roaming_wolf", point, true, nil, nil, DOTA_TEAM_BADGUYS)
		local waypoint = Entities:FindByName(nil, "wolf7")
		unit:SetInitialGoalEntity( waypoint )
				local unit = CreateUnitByName("roaming_wolf", point, true, nil, nil, DOTA_TEAM_BADGUYS)
		local waypoint = Entities:FindByName(nil, "wolf7")
		unit:SetInitialGoalEntity( waypoint )
				local unit = CreateUnitByName("roaming_wolf", point, true, nil, nil, DOTA_TEAM_BADGUYS)
		local waypoint = Entities:FindByName(nil, "wolf7")
		unit:SetInitialGoalEntity( waypoint )
				local unit = CreateUnitByName("roaming_wolf", point, true, nil, nil, DOTA_TEAM_BADGUYS)
		local waypoint = Entities:FindByName(nil, "wolf7")
		unit:SetInitialGoalEntity( waypoint )
		end
        
    
end



-- This function initializes the game mode and is called before anyone loads into the game
-- It can be used to pre-initialize any values/tables that will be needed later
function junglebash:Initjunglebash()
  junglebash = self
  DebugPrint('[JUNGLEBASH] Starting to load junglebash junglebash...')

  -- Commands can be registered for debugging purposes or as functions that can be called by the custom Scaleform UI
  Convars:RegisterCommand( "command_example", Dynamic_Wrap(junglebash, 'ExampleConsoleCommand'), "A console command example", FCVAR_CHEAT )

  DebugPrint('[JUNGLEBASH] Done loading junglebash junglebash!\n\n')
end

-- This is an example console command
function junglebash:ExampleConsoleCommand()
  print( '******* Example Console Command ***************' )
  local cmdPlayer = Convars:GetCommandClient()
  if cmdPlayer then
    local playerID = cmdPlayer:GetPlayerID()
    if playerID ~= nil and playerID ~= -1 then
      -- Do something here for the player who called this command
      PlayerResource:ReplaceHeroWith(playerID, "npc_dota_hero_viper", 1000, 1000)
    end
  end

  print( '*********************************************' )
end
